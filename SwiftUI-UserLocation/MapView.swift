//
//  ContentView.swift
//  SwiftUI-UserLocation
//
//  Created by Massimiliano Bonafede on 16/09/22.
//

import MapKit
import SwiftUI

struct MapView: View {
    @StateObject private var viewModel = MapViewModel()
   
    
    var body: some View {
        Map(coordinateRegion: $viewModel.region,
            showsUserLocation: true)
        .ignoresSafeArea()
        .accentColor(Color(.systemPink))
        .onAppear {
            viewModel.checkIfLocationServicesIsEnabled()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
