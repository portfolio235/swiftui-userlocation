//
//  SwiftUI_UserLocationApp.swift
//  SwiftUI-UserLocation
//
//  Created by Massimiliano Bonafede on 16/09/22.
//

import SwiftUI

@main
struct SwiftUI_UserLocationApp: App {
    var body: some Scene {
        WindowGroup {
            MapView()
        }
    }
}
