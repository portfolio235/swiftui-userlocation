//
//  MapViewModel.swift
//  SwiftUI-UserLocation
//
//  Created by Massimiliano Bonafede on 16/09/22.
//

import MapKit
import SwiftUI

final class MapViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    @Published var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(
            latitude: 37.331516,
            longitude: -121.891054),
        span: MapViewModel.span)
    
    private static let span = MKCoordinateSpan(
        latitudeDelta: 0.1,
        longitudeDelta: 0.1)
    var locationManager: CLLocationManager?
    
    func checkIfLocationServicesIsEnabled() {
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    private func checkLocationAuthorization() {
        guard let locationManager = locationManager else { return }
        switchLocationAuth(locationManager)
    }
    
    private func switchLocationAuth(_ locationManager: CLLocationManager) {
        switch locationManager.authorizationStatus {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            
        case .restricted:
            print("Location is Restricted")
            
        case .denied:
            print("Location is Denied, go in setting to change it")
            
        case .authorizedAlways, .authorizedWhenInUse:
            region = MKCoordinateRegion(
                center: locationManager.location!.coordinate,
                span: MapViewModel.span)
            
        @unknown default: break
        }
    }
        
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status != .denied else {
            print("Turn on your location in app settings")
            return
        }
        checkLocationAuthorization()
    }
}
